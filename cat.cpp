///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   05/05/2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}


void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}



void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}





void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder( topCat );
}

bool CatEmpire::empty() {

	if (topCat == nullptr) {
		return true;
	}
	return false;
}

void CatEmpire::addCat(Cat* newCat) {

	if (newCat == nullptr) {
		cout << "Null Cat!" << endl;
		return;		
	}

	if (empty()) {
		topCat = newCat;
		topCat->left = nullptr;
		topCat->right = nullptr;
		return;
	}

	else {
		addCat(topCat, newCat);
		return;
	}
}

void CatEmpire::addCat (Cat* atCat, Cat* newCat) {

	if (newCat == nullptr) {
                cout << "Null Cat!" << endl;
                return;
        }

	if (atCat->name > newCat->name) {
		if (atCat->left == nullptr) {
			atCat->left = newCat;
		}
		if (atCat->left != nullptr) {
			addCat (atCat->left, newCat);
		}
	}

	if (atCat->name < newCat->name) {
                if (atCat->right == nullptr) {
                        atCat->right = newCat;
                }
                if (atCat->right != nullptr) {
                        addCat (atCat->right, newCat);
                }
        }

	return;
}


const bool CatEmpire::validate() const {
                unsigned int count = 0;
		Cat* head;
		head = topCat;

		while (head != nullptr) {
			count++;
			head = head->right;
		}

		head = topCat;
		count--;

		while (head != nullptr) {
                        count++;
                        head = head->left;
                }

		cout << "size of list is = " << count << endl;

		if (topCat == nullptr) {
			assert( left == nullptr);
			assert( right == nullptr);
			assert( count == 0);
                }
		
		return true;

	}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const {

	if (atCat == nullptr) {
		//cout << "tree is empty!" << endl;
		return;
	}

	dfsInorderReverse( atCat->right, (depth+1));

   
	cout << string(6*(depth-1), ' ') << atCat->name;
   
	if (atCat->left != nullptr && atCat->right != nullptr) {
		cout  << "<" << endl;
	}

	if (atCat->left != nullptr && atCat->right == nullptr) {
		cout  << "\\" << endl;
	}

	if (atCat->left == nullptr && atCat->right != nullptr) {
		cout << "/" << endl;
	}

	if (atCat->left == nullptr && atCat->right == nullptr) {
		cout << endl;
	}

	dfsInorderReverse( atCat->left, (depth+1) );


}

void CatEmpire::dfsInorder( Cat* atCat ) const {

	if (atCat == nullptr) {
                //cout << "tree is empty!" << endl;
                return;
        }

	dfsInorder(atCat->left);
	cout << atCat->name << endl;
	dfsInorder (atCat->right);
	return;
}

void CatEmpire::dfsPreorder( Cat* atCat ) const {

	if (atCat == nullptr) {
                //cout << "tree is empty!" << endl;
                return;
        }

	if (atCat->left != nullptr && atCat->right != nullptr) {
		cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
	}

	if (atCat->left != nullptr && atCat->right == nullptr) {
		cout << atCat->name << " begat " << atCat->left->name << endl;
	}

	if (atCat->right != nullptr && atCat->left == nullptr) {
                cout << atCat->name << " begat " << atCat->right->name << endl;
        }
	
	dfsPreorder( atCat->left );

	dfsPreorder( atCat->right );
}

void CatEmpire::catGenerations() const {

	int gen = 1;
	int x = 1;
	int y = 0;

	getEnglishSuffix(gen);
	cout << "  ";
	queue<Cat*> catQueue;
	catQueue.push( topCat );

	while (catQueue.empty() == false) {
		Cat* cat = catQueue.front();
		catQueue.pop();

	if (x == 0) {
        	gen++;
        cout << endl;
        getEnglishSuffix(gen);
        cout  << "  ";
        x = y;
        y = 0;
        }

        if (cat == nullptr)
        	return;
      
        if (cat->left != nullptr) {
        	catQueue.push( cat->left );
        	y++;
	}
      
	if (cat->right != nullptr) {
        	catQueue.push( cat->right );
        	y++;
	}

	cout << cat->name << "  ";
	x--;
   
	}
   
	cout << endl;


}

void CatEmpire::getEnglishSuffix( int n ) const {

	if (n%100 != 11 && n%100 != 12 && n%100 != 13) {
		if (n%10 == 1) { 
			cout << n << "st" << "Generation" << endl;
			return;
		}

		if (n%10 == 2) {
			cout << n << "nd" << "Generation" << endl;
			return;
		}

		if (n%10 == 3) {
			cout << n << "rd" << "Generation" << endl;
			return;
		}
	}
	
	cout << n << "th" << "Generation" << endl;
	return;
}
