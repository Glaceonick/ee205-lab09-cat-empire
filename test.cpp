///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file test.cpp
/// @version 1.0
///
/// Test a family tree of cats
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   05/05/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "cat.hpp"

using namespace std;

int main() {

	CatEmpire kitty;

	cout << "empty? " << boolalpha << kitty.empty() << endl;

	kitty.addCat(new Cat ("meow"));

	cout << "added meow" << endl << "empty? " << boolalpha << kitty.empty() << endl;

	kitty.validate();

	kitty.addCat(new Cat ("whisker"));

        cout << "added whisker" << endl << "empty? " << boolalpha << kitty.empty() << endl;

	kitty.validate();

}
